var args = arguments[0] || {};
var psAnimation = require('animation');
var __ = require('platformSupport');
var fontIconLoader = require("icomoonlib");
var dialogBox = require("psdialog");
var loadingWindow = require('loadingWindow');
var loader = require("loader");
var basket = Alloy.Collections.basket;
var myBasket;
var totalAmount = 0;
var checkout_shop_id = 0;
var attributeString = "";
var click = 1;
var lang = require("language");

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}

var init = function() {
	$.mainTitle.text = lang.L("CardCheckout");
	loadIcon();
	$.ScrollView.top = Ti.Platform.osname == "android" ? '44dp' : '64dp';
	doCardPayment();
};

var loadIcon = function() {
	$.imgBack.image = fontIconLoader.getIcon("panacea", "back", 35, {
		color : Alloy.CFG.Colors.IconWhite
	});
};

var closeWindow = function() {
	psAnimation.out($.stripe);
	$.paypal = null;
};

var doCardPayment = function() {
	var webView = Ti.UI.createWebView({
		top : Ti.Platform.osname == "android" ? '44dp' : '64dp',
		url : Ti.App.Properties.getString('SETTING_LANGUAGE') == "en" ? '/stripeT3.html' : '/stripeT.html',
		scalesPageToFit : true,
	});

	$.stripe.add(webView);

	$.stripe.addEventListener('close', function(e) {
		Ti.App.removeEventListener('app:fromTitanium', callPayment);
		Ti.App.removeEventListener('saveCardDetails', saveCardDetails);
		Ti.App.removeEventListener('removeCardDetails', removeCardDetails);
		Ti.App.removeEventListener('saveRememberMe', saveRememberMe);
		//Ti.App.removeEventListener('loadLanguage', saveRememberMe);
		$.stripe.remove(webView);
		webView = null;

	});

	Ti.App.addEventListener('app:fromTitanium', callPayment);
	Ti.App.addEventListener('saveCardDetails', saveCardDetails);
	Ti.App.addEventListener('removeCardDetails', removeCardDetails);
	Ti.App.addEventListener('saveRememberMe', saveRememberMe);

	webView.addEventListener("load", function() {
		Ti.API.info('Publishable Key  ' + Ti.App.Properties.getString("selected_shop_stripe_publisher_key"));

		Ti.App.fireEvent("getPublisherKey", {
			key : Ti.App.Properties.getString("selected_shop_stripe_publisher_key")
		});

		get_CardDetails();

	});

};

var removeCardDetails = function() {
	Ti.App.Properties.setObject("cardDetails", null);
};

var saveRememberMe = function(e) {
	Ti.App.Properties.setBool("rememberMe", e.value);
};

var saveCardDetails = function(e) {

	Ti.App.Properties.setObject("cardDetails", {
		cardNum : e.cardNum,
		ExpDate : e.expDate,
		cvvNum : e.cvvNum
	});

	var details = Ti.App.Properties.getObject("cardDetails");
	//Ti.API.info('details  -- ' + JSON.stringify(details));
};

var get_CardDetails = function(e) {
	var details = Ti.App.Properties.getObject("cardDetails");

	if (Ti.App.Properties.getBool("rememberMe")) {
		Ti.App.fireEvent("getCardDetails", {
			cardNum : details.cardNum,
			ExpDate : details.ExpDate,
			cvvNum : details.cvvNum,
			value : Ti.App.Properties.getBool("rememberMe")
		});

	}
};

var callPayment = function(e) {
	if (click == 1) {
		click = 0;

		//Ti.API.info('token:  ' + e.message);

		myBasket = Alloy.createCollection('basket');
		myBasket.fetch();
		var data = myBasket.where({
			"shop_id" : parseInt(Ti.App.Properties.getString("selected_shop_id"))
		});

		console.log(" Total Records To Send Server : " + data.length);

		var transactions = {};
		var orders = [];

		for (var i = 0; i < data.length; i++) {
			checkout_shop_id = data[0].get("shop_id");

			orders.push({
				"item_id" : data[i].get("item_id"),
				"shop_id" : data[i].get("shop_id"),
				"unit_price" : parseFloat(data[i].get("unit_price")), // parseInt(data[i].get("unit_price")),
				"discount_percent" : data[i].get("discount_percent") || 0,
				"name" : data[i].get("name"),
				"qty" : data[i].get("qty"),
				"user_id" : data[i].get("user_id"),
				"paypal_trans_id" : "",
				"delivery_address" : args.tableNumber,
				"billing_address" : args.billing_address,
				"order_total_amount" : parseFloat(args.amount), //parseInt(args.amount),
				"basket_item_attribute" : itemGotAttribute(data[i].get("item_id"), data[i].get("shop_id")),
				"payment_method" : "card",
				"email" : args.email,
				"phone" : args.phone,
				"coupon_discount_amount" : args.coupon_discount_amount || 0,
				"flat_rate_shipping" : Ti.App.Properties.getString('selected_shop_flat_rate_shipping'),
				"stripe_token" : e.message,
				"stripe_private_key" : Ti.App.Properties.getString("selected_shop_stripe_secret_key")//"sk_test_V90XIzBt4U5ZtKSa8qs5YxLx",
			});

			attributeString = "";
		}

		Alloy.Globals.shopID = data[0].get("shop_id");
		transactions.orders = orders;
		console.log(JSON.stringify(transactions));
		loadingWindow.startLoading();

		console.log("+++++ just before the calback funtion ++++++");
		var apiArgs = {
			callbackFunction : callBackSuccessPayment,
			payload : transactions,
			url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postTransactionsData,
			loadingWindow : loadingWindow
		};

		loader.post(apiArgs);
	}
};

var callBackSuccessPayment = function(respData) {
	click = 1;
	loadingWindow.endLoading();

	if (respData != null) {
		if (respData.success != "") {
			psAnimation.close($.stripe);

			var contentView = Alloy.createController("payment/success").getView();
			psAnimation.in(contentView);

			console.log("+++++ Already Clean Basket ++++++");
			cleanBasket();

			console.log("+++++ Already Clean Item Attribute ++++++");
			cleanAttribute();

			console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Item Detail ++++++++++");
			var itemCountRefresh = Alloy.Globals.itemCountRefresh;
			itemCountRefresh();

			console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Item List ++++++++++");
			var itemCountRefreshItemList = Alloy.Globals.itemCountRefreshItemList;
			itemCountRefreshItemList();

			console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Index ++++++++++");
			var itemCountRefreshIndex = Alloy.Globals.itemCountRefreshIndex;
			var params = {
				selected_shop_id : checkout_shop_id
			};
			itemCountRefreshIndex(params);

			console.log("+++++ Already Close All Window From Background +++++++++ ");
			var closeCheckout = Alloy.Globals.closeCheckout;
			closeCheckout();
			var closeBasket = Alloy.Globals.closeBasket;
			closeBasket();
			var closeItemDetail = Alloy.Globals.closeItemDetail;
			closeItemDetail();
			var closeItemList = Alloy.Globals.closeItemList;
			closeItemList();
			var closePaymentOption = Alloy.Globals.closePaymentOption;
			closePaymentOption();

			var closeReorder = Alloy.Globals.closeReorder;
			closeReorder();

			if (Alloy.Globals.close_PaymentOption) {
				var close_PaymentOption = Alloy.Globals.close_PaymentOption;
				close_PaymentOption();
			}

			if (Alloy.Globals.closeOrderHistory)
				Alloy.Globals.closeOrderHistory();

			if (Alloy.Globals.singleShop_itemCount) {
				Alloy.Globals.singleShop_itemCount({
					selected_shop_id : Alloy.Globals.shopID
				});
			}
		}
	}
};

var itemGotAttribute = function(item_id, shop_id) {
	var item_attribute = Alloy.createCollection('item_attribute');
	item_attribute.fetch();
	var data = item_attribute.where({
		"item_id" : item_id
	});
	for (var i = 0; i < data.length; i++) {
		if (data.length - 1 == i) {
			attributeString += data[i].get("header") + " : " + data[i].get("name");
		} else {
			attributeString += data[i].get("header") + " : " + data[i].get("name") + ", ";
		}
	}
	console.log("Before Return attributeString : " + attributeString);
	return attributeString;
};

var cleanBasket = function() {
	//Alloy.Collections.basket.deleteAll();
	Alloy.Collections.basket.deleteRecord({
		query : {
			sql : "WHERE shop_id=?",
			params : checkout_shop_id
		}
	});
};

var cleanAttribute = function() {
	Alloy.Collections.item_attribute.deleteRecord({
		query : {
			sql : "WHERE shop_id=?",
			params : checkout_shop_id
		}
	});
};

init();